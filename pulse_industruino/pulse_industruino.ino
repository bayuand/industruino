#include <Indio.h>
#include <Wire.h>

#include <UC1701.h>
static UC1701 lcd;

volatile int pulse = 0;

//backlight
const int backlightPin = 26; // PWM output pin that the LED backlight is attached to
int backlightIntensity = 200;        // LCD backlight intesity


/* parameter */
float max_pulse = 50000;
float min_pulse = 5000;//5000
float factor = 10000;//10000
float percent;
void setup() {

  SerialUSB.begin(9600);

  pinMode(backlightPin, OUTPUT); //set backlight pin to output
  analogWrite(backlightPin, (map(backlightIntensity, 5, 1, 255, 0))); //convert backlight intesity from a value of 0-5 to a value of 0-255 for PWM.
  lcd.begin();

  Indio.digitalMode(1, OUTPUT); //  Clear CH1 to LOW
  Indio.digitalWrite(1, LOW);  // Clear CH1 to LOW
  Indio.digitalMode(1, INPUT); // Set CH1 as an input

  attachInterrupt(8, count, RISING);       // D8 attached to the interrupt of the expander
  attachInterrupt(24, enter, FALLING);      // D24 is the Enter button (pull-up)
  attachInterrupt(25, up, FALLING);      // D25 is the Up button (pull-up)
  attachInterrupt(23, down, FALLING);      // D23 is the Down button (pull-up)

}

void count() {
  pulse++;
}

void enter() {
  lcd.clear();
  lcd.setCursor(1, 6);
  lcd.print("enter");
}

void up() {
  lcd.clear();
  lcd.setCursor(1, 6);
  lcd.print("up");
}

void down() {
  lcd.clear();
  lcd.setCursor(1, 6);
  lcd.print("down");
}

void loop() {
  //  interrupts();
  lcd.clear();
  lcd.setCursor(1, 2);
  lcd.print("Pulse = ");
  lcd.setCursor(50, 2);
  lcd.print(pulse);
  lcd.setCursor(1, 4);
  percent = count_percent(pulse);
  lcd.print("Percent = ");
  lcd.setCursor(50, 4);
  lcd.print(percent);
  //  noInterrupts();
  pulse = 0;
  delay(1000);
}


float count_percent(unsigned int val) {
  if (val > max_pulse) {
    val = max_pulse;
  } else if (val < min_pulse) {
    val = min_pulse;
  }
  return ((max_pulse / val) - 1) / ((max_pulse - min_pulse) / factor);
}
